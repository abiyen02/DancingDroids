use std::fs::File;
use std::io::Read;
enum Instruction {
    L,
    R,
    F,
}
pub enum Orientation {
    E,
    W,
    N,
    S,
}
// Une structure qui gere les Intructions
struct Inst;
impl Instruction {
    pub fn instructions(self, instructions: &str) -> Self { 
        let mut robot = self; 
        for instruction in instructions.chars() { 
         robot = match instruction { 
          'L' => { robot.go_straight() }, 
          'R' => { robot.turn_right() }, 
          'F' => { robot.turn_left() }, 
          _ => { 
           println!("linstruction {} nest pas valide ", instruction); 
           robot 
          }, 
         }; 
        } 
        robot 
    }  
}
//  Fonction permettant de lire les caractere entre par l'ordinateur
struct ErrorInvalidOrientation;
impl Orientation {
    fn try_from(lettre: char) -> Result<Orientation, ErrorInvalidOrientation> {
        match lettre {
            'E' => Ok(Orientation::E),
            'W' => Ok(Orientation::W),
            'N' => Ok(Orientation::N),
            'S' => Ok(Orientation::S),
            _ => Err(ErrorInvalidOrientation),
        }
    }
}
// Cette structure permet de gere lerobot
struct Robot {
    new_id: i32,               // l'identifiant du robot
    x: i32,                    //'abscisse fait référence à l'axe horizontal (x)
    y: i32,                    // l'ordonnée fait référence à l'axe vertical (y)
    orientation: Orientation,  // le direction du robot
    position,
    instruc: Vec<Instruction>, // les instruction reçu par l'ordinaeur
}
/// Donne les coordonne de depart du robot et le permet de bouger
/// Aide reçu sur la plateforme stackoverrun ET des groupes Discord = RUST
impl Robot {
    pub fn move_robot(x: isize , y:isize, o:Orientation) -> Self {
        Robot { position: Coordinate { x:x , y:y}, o: orientation}
    } 
    ///Fonction qui permet de deplace le robot a Droite
    pub fn turn_right(&mut self) -> Self { 
        match self.orientation { 
            Orientation::E => self.orientation = Orientation::N,
            Orientation::N => self.orientation = Orientation::W,
            Orientation::W => self.orientation = Orientation::S,
            Orientation::S => self.orientation = Orientation::E,
        },self
    }
    /// Fonction qui permet de deplace le robot a Gauche
    pub fn turn_left(&mut self) -> Self { 
        match self.orientation { 
            Orientation::E => self.orientation = Orientation::S,
            Orientation::N => self.orientation = Orientation::E,
            Orientation::W => self.orientation = Orientation::N,
            Orientation::S => self.orientation = Orientation::W,
        },self
    }
    /// Fonction qui permet d'aller dans la direction ou le robot fais face
    pub fn go_straight (&mut self ) -> Self{
        match self.orientation {
            Orientation::E => self.x = self.x + 1,
            Orientation::N => self.y = self.y + 1,
            Orientation::W => self.x = self.x - 1,
            Orientation::S => self.y = self.y - 1,
            },
        }self
    }
    /// verfie si il y'a eu colission entre le robot et un autre robot
    /// exmplaire de code pris sur RUST DOC DE GUILLAUME GOMEZ
    pub fn collision (&self, other_robot: &Robot) -> bool{
        self.x == other_robot.x && self.y > other_robot.y && self.new_id != other_robot.new_id
        println! ("Robot ID {} Collision en ({}, {})",new_id,x,y)
    }
    /// Affiche les instruction fournie par l'ordinateur
    pub fn affiche {
        let forward = Instruction::go_straight('F');
        let left = Instruction::turn_left('L');
        let right = Instruction::turn_right('R');
        println!(" identifiant :{:#?} ,Orientation : {:#?} , position {:#?}{:#?} ", new_id, o,x,y )
    }

}///il y'a eu une faute ici , mais je ne comprens pas pourquoi
/// Permet de faire des test sur les Orientation
#[cfg(test)]
mod tests {
    #[test]
    fn test_orientation() {
        assert!(matches!(Orientation("E"), Orientation::E);
        assert!(matches!(Orientation("W"), Orientation::W);
        assert!(matches!(Orientation("N"), Orientation::N);
        assert!(matches!(Orientation("S"), Orientation::S);
    }

}  
/// Permet de faire des test sur les instruction
 fn #[test]
    fn test_instruction() {
        assert!(matches!(Instruction("L"), Instruction::L));
        assert!(matches!(Instruction("R"), Instruction::R));
        assert!(matches!(Instruction("F"), Instruction::F));

    }

///ouvrir un fichier txt " TWO_ROBOT.TXT"
let mut fichier = match File::open("two_robots.txt") {
    Ok(f) => {
        f
    }
    Err(e) => {
        println!("{}", e);
        return;
    }
};

///Desole j'ai pas pu faire cargo fmt parce qu'il y'avait une faute
fn main() {

    
}
